---
layout: markdown_page
title: "Commercial Sales"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Welcome to the Commercial Sales Handbook

### About 

The Commercial Sales department is part of the overall [GitLab Sales](/handbook/sales) functional group. We focus on [SMB and Mid-Market](/handbook/business-ops/#segmentation) customers to deliver maximum value throughout the entire journey with GitLab.

### Center of Excellence

The Center of Excellence (COE) in the Commercial Sales context refers to the team's research and development of a particular focus area to drive new initiatives and improvements across the organization.  

Why call it Center of Excellence? 
- We have challenges we are uniquely positioned to focus on
- We aren’t the Directly Responsible Individual (DRI) but it is a challenge to get this prioritized
- This is our current platform for collaboration

How?
1. Choose one of the active Topics currently available below. Refer to the team list to see what is unassigned.  Note, to take a topic you do not have to be a Subject Matter Expert (SME) in the area or DRI as they may belong to a different functional group. 
2. Pick an achievable set of commitments and nail it. These should be forecastable and not something too aspirational for the timeframe
3. Decide for yourself if you want that same topic for 3 or 6 months
4. Start with the problem related to your subject that you think needs to be solved

#### Commercial COE Topics
Here are the current active topics for individual team members on COE alignment. This center in its first iteration started with the team's QBR in 2019. 

- **Communication and Collaboration** 
Support on communication and tools to stay better connected as a remote Commercial Sales organization
- **Customer Portal** 
Improvements to our [customer portal](https://customers.gitlab.com)
- **Customer Advisory Board** 
Capturing direct feedback for interaction and organizing customers selected
- **Feedback Loop** 
Improvements to feedback channels to document structured data for all customer interactions
- **Hosted gitlab.com** 
Ramping new customers and users on gitlab.com, our hosted product offering
- **KISS enablement** 
Keep it simple. Focus on process including managing tasks, training GitLab, and new release features
- **Marketing campaigns**
- **Sales content/collateral**
- **Sales process - BDR**
Improvements to the BDR to AE collaboration and working agreement
- **Sales process - SDR**
Improvements to the SDR to AE collaboration process
- **Services**
Improvements to the Professional Services offering
- **Ultimate**
Improvements to the full pitch including [Why Ultimate](https://about.gitlab.com/pricing/ultimate/)

## <i class="fas fa-users fa-fw icon-color font-awesome" aria-hidden="true"></i> Commercial Team Groups
- SMB Customer Advocates
- Account Executives
- Area Sales

## Other Related Pages
- [Commercial Sales - Customer Success](/handbook/customer-success/comm-sales)
- [Business Operations Territories](/handbook/business-ops/#territories)
- [Sales Hiring Chart](/handbook/hiring/charts/sales)